# Requirement dependency
* terraform
* pipenv for python 3.8
* aws-cli

---

# Create a EC2 then provision minikube on EC2
### create s3 bucket for terraform store state file
```bash
aws s3 mb s3://<BUCKET-FOR-TF>/
aws s3api put-object --bucket <BUCKET-FOR-TF> --key newyorker
```
### replace the "BUCKET-FOR-TF" in main.tf line 3 with "tf-poc"

### init with terraform
```bash
terraform init
terraform workspace new dev
```

### provision with terraform and ansible
```
terraform apply
```

### [Option] only provision with ansible.
```
ansible-playbook -i ./ansible/inventory --limit nodes ./ansible/minikube.yml -u ubuntu
```

# Build up the tunnel to access minikube
```
ssh -A -D 1080 -L 8443:172.17.0.3:8443 -C -q ubuntu@<PUBLIC_EIP>
```

# add ansible/aws/aws-kubeconfig into KUBECONFIG

# Deploy gitlab with helm
```
helm repo add gitlab https://charts.gitlab.io/
helm repo update
helm upgrade --install gitlab gitlab/gitlab \
    --timeout 600s \
    -f https://gitlab.com/gitlab-org/charts/gitlab/raw/master/examples/values-minikube-minimum.yaml \
    --set global.hosts.domain=<PUBLIC_EIP>.nip.io 
    --set global.hosts.externalIP=<PUBLIC_EIP> 
    -f helm/gitlab-value.yaml
```

---


# CI/CD with app
* build up app in app directory, it run through test -> build -> release -> deploy process.
* setup the following environment variables in CI/CD's Variables 
  - DOCKER_REGISTRY = registry-1.docker.io
  - DOCKER_REGISTRY_PASSWORD = *DOCKER-TOKEN*
  - DOCKER_REGISTRY_USER = titanlien
---

# TODO:
* Fix the minikube's SSL certificate, because the runner can't use self signed SSL.
* Registry a runner
* In CI/CD pipeline, deploy with helm not finish
