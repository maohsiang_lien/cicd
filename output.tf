output "spot_instance_private_ip" {
  value = module.worker.spot_instance_private_ip
}

output "spot_instance_ids_worker" {
  value = module.worker.spot_instance_ids_worker
}

output spot_instance_public_ip {
  value = module.worker.spot_instance_public_ip
}
