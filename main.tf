terraform {
  backend "s3" {
    # TODO: need to be switched your bucket
    bucket = "tf-poc"
    key    = "newyorker/terraform.tfstate"
    region = "eu-central-1"
  }
}

locals {
  regions = {
    dev        = "eu-central-1"
    staging    = "eu-central-1"
  }

  worker_instance_count = {
    dev        = "1"
    staging    = "1"
  }
}

module "worker" {
  source           = "./worker"
  region           = local.regions[terraform.workspace]
  number_instances = local.worker_instance_count[terraform.workspace]
  vpc              = module.vpc
  sg-id            = aws_security_group.allow_task1.id
}

resource "null_resource" "ansible-install-minikube" {
  triggers = {
    provision = local.worker_instance_count[terraform.workspace]
  }
  provisioner "local-exec" {
    # wait the default apt-get finishing
    command = "sleep 300; ansible-playbook -i ./ansible/inventory --limit nodes ./ansible/minikube.yml -u ubuntu -b"
  }

  depends_on = [module.worker]
}
